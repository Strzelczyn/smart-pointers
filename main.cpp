#include <iostream>
#include <memory>

int main() {
  int size = 3;
  // can't be copy but can be move and auto delete
  std::unique_ptr<int[]> tab(new int[size]);
  std::unique_ptr<int[]> tab2;
  for (int i = 0; i < size; ++i) {
    tab[i] = i;
  }
  std::cout << tab[2] << std::endl;
  std::cout << "uptr: " << (tab != nullptr) << " uptr2: " << (tab2 != nullptr)
            << '\n';
  tab2 = std::move(tab);
  std::cout << "uptr: " << (tab != nullptr) << " uptr2: " << (tab2 != nullptr)
            << '\n';

  // can be copy and the last one will delete
  std::shared_ptr<int> sptr(new int);
  std::shared_ptr<int> sptr2;
  std::cout << "sptr: " << sptr.use_count() << " sptr2: " << sptr2.use_count()
            << '\n';
  sptr2 = sptr;
  std::cout << "sptr: " << sptr.use_count() << " sptr2: " << sptr2.use_count()
            << '\n';
            
  // weak reference on shared_ptr easy to delete
  std::weak_ptr<int> weak;
  std::cout << "expired: " << weak.expired() << "; address: " << weak.lock()
            << '\n';
  {
    std::shared_ptr<int> strong(new int);
    weak = strong;
    std::cout << "expired: " << weak.expired() << "; address: " << weak.lock()
              << '\n';
  }
}
